# README #

Configuration for the HTCondor service at the Institute for Experimental Particle Physics/Karlsruhe Institute of Technology.

### What is this repository for? ###

This repo contains all configuration files for the EKP HTCondor service.
This includes the production system as well as all development resources.
Only Administrators of the service bother with this repository.

The configurations have been tested with HTCondor 8.X on Scientific Linux 6.

### How do I get set up? ###

#### Summary ####

* Install HTCondor on a node
* Download this repository
* Deploy configs to the configuration directory
* Install the security settings
* Run the HTCondor node

#### Detailed Setup ####

* Install HTCondor on a node
    * Ideally, install from the official [HTCondor repositories](http://research.cs.wisc.edu/htcondor/yum/).
    * On SL6, you may have to disable UMD-3 during installation via `yum install condor --disablerepo=UMD-3-base`.
* Download this repository
    * You will want to use only a portion of the configurations.
      Do not download to the HTCondor configuration directory directly.
    * On SL6, the suggested location is `/etc/condor/ekpcondor/`.
* Deploy configs to the configuration directory
    * Exclude the configs for services you do not need.
      Move all other to HTCondor's configuration directory.
    * On SL6, the default location is `/etc/condor/config.d/`.
    * You can query the config location via `condor_config_val LOCAL_CONFIG_DIR`.
* Install the security settings
    * Fetch the security settings (password, mapfile, etc.) from a trusted location.
* Run the HTCondor node

### Contribution guidelines ###

The configuration files use a hierarchical approach.
This starts at general settings and moves down to optional settings for specific purposes.
Please try to keep general settings in the top-most file that is appropriate.

* Adding Documentation
    * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Who do I talk to? ###

* For general EKP HTCondor questions ask [Max Fischer](max.fisher@kit.edu)
* For specific projects/changes, contact the respective submitters