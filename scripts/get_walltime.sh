#!/usr/bin/bash
# ===============================================================================
# Copyright (c) 2016 by Frank Fischer
# ===============================================================================

default_file="/root/wall_time"
default_file_fb="/var/log/condor/MasterLog"

function usage() {
  echo "Usage: $0 [-fb]

  Read machine boot time & wall time from a file.

  File content must be a single line containing the machine's wall time
  in unix time. The file's creation will be used to determine the
  machine's boot time.

  The information is output via echo in the format
  Machine_Walltime=number in seconds
  Machine_Starttime=unix time
  so condor can use the values.

  Options:
  -f Path to the file containing the wall time information (created
     upon booting the machine via cloud-init).
     Default: $default_file

  -b Fallback file to ONLY determine machine start time.
     Default: $default_file_fb" >&2
}

function get_walltime() {
    local file="$1"
    local file_fb="$2"
    if [ -f $file ]; then
        # Read file content
        machine_wall_time=$(<$file)
    else
        echo "# Problem with $file."
        echo "# Using $file_fb creation date."
        file=$file_fb
        machine_wall_time="Undefined"
    fi

    machine_start_time=$(stat -c %Y $file)

    # Output information to stdout; this is parsed by condor config
    echo "Machine_Walltime=$machine_wall_time"
    echo "Machine_Starttime=$machine_start_time"
}

# Read input variables
while getopts ":f:b" opt; do
  case $opt in
    f)
      file="$OPTARG"
      ;;
    b)
      file_fb="$OPTARG"
      ;;
    \?)
      echo -e "Invalid option: -$OPTARG\n" >&2
      usage
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument.\n" >&2
      usage
      exit 1
      ;;
  esac
done

# Set defaults to non-given argument variables
for variable in file file_fb; do
  eval "if [ -z \"\$$variable\" ]; then
    $variable=\"\$default_$variable\"
  fi"
done

# execution
get_walltime "$file" "$file_fb"

exit 0
