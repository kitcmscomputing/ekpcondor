#!/usr/bin/bash
# ===============================================================================
# Copyright (c) 2016 by Frank Fischer
# ===============================================================================

# https can not be cached, but works flawlessly with the smart protocol.
# http uses the old dumb protocol and may encounter timeouts
default_git_remote='https://bitbucket.org/kitcmscomputing/ekpcondor.git'
default_git_refspec='master'
default_config_dir='/etc/condor/config.d/'
default_log_file='/var/log/chtc_htcondor_config_git_pull.log'
logger_tag=htcondor_git_pull
default_site_name=''
default_file_list='/tmp/git_list'

PYTHON_SCRIPT="config_file_list.py"
JSON_FILE="config_sites.json"

REGEX_STRING="^script:(.+):(.+)$"

function usage() {
  echo "Usage: $0 [-rbdfsl]

  This command is meant to pull HTCondor configuration
  from a remote repository during a condor_reconfig.
  It also spits out a CONFIG_HASH assignment so that
  HTCondor can report the hash of it's working HEAD.

  Options:

  -r The name of the git repository remote.
     Default: $default_git_remote

  -b The refspec (or branch) that git will use to update
     the local branch.  The -r option must be used for
     the -b option to have any effect.
     Default: $default_git_refspec

  -d The path of the local git repository (HTCondor
     configuration directory).
     Default: $default_config_dir

  -f file list which contains the name of all corresponding
     config files.
     Default: $default_file_list

  -s JSON file key value (for which site to build the config file list).

  -l The path of the log file for this script. Syslog
     is used by default." >&2
}

function git_commit_del(){
    # commit (potential) local config file deletions to local git.
    if [[ -n $(git status -s) ]]; then
        git commit -qa -m "Commit local config deletions."
    fi
}

function do_git_sync() {
    local git_remote="$1"
    local git_refspec="$2"
    local file_list="$3"
    local logger="$4"

    if [ -z "$logger" ]; then
        date +"%F %H:%M:%S Attempting git pull in $config_dir"
    else
        echo "Attempting git pull in $config_dir"
    fi

    # init repo (safe to do in regular repo)
    git init -q

    # Initial setup for empty directory to bring master up to date with origin master
    if [[ -z $(git remote) ]]; then
        echo "Adding git remote origin $git_remote"
        git remote add -m $git_refspec -f origin $git_remote
        # link remote branch to local branch (tracking);
        # from now on, operate on local branch to keep tracking.
        git branch $git_refspec origin/$git_refspec
        if [ -e $file_list ]; then
            for line in $(<$file_list); do
                if [[ $line =~ $REGEX_STRING ]]; then
                    echo "Checking out ${BASH_REMATCH[2]} & and linking to ${BASH_REMATCH[1]}"
                    git checkout -fq $git_refspec -- ${BASH_REMATCH[2]}
                    ln -fs "$( pwd )/${BASH_REMATCH[2]}" "${BASH_REMATCH[1]}"
                else
                    echo "Checking out ${line}"
                    git checkout -fq $git_refspec -- $line
                fi
            done
            git_commit_del
        else
            echo "No filelist found, checking out the whole repository."
            git checkout -fq $git_refspec
            # Delete readme file
            rm -f README.md
        fi
    else
        echo "Clean & reset repository + pull."
        git clean -q -fd    && \
        git reset -q --hard && \
        git pull  -q -f \
        ${git_remote:+$git_remote} \
        ${git_remote:+${git_refspec:+$git_refspec}}
        # Delete readme file
        rm -f README.md
    fi

    rc=$?
    git_head="$(git rev-parse HEAD 2>/dev/null)"
    if [ "$rc" -eq 0 ]; then
        echo "Success, HEAD is $git_head"
    else
        echo "Failure"
    fi
    return $rc
}

function create_config_list() {
    # python parse input JSON file into output file
    local site_name="$1"
    local file_list="$2"
    python "$PYTHON_SCRIPT" --out "$file_list" "$JSON_FILE" "$site_name"
}

while getopts ":b:d:l:r:s:f" opt; do
  case $opt in
    r)
      git_remote="$OPTARG"
      ;;
    b)
      git_refspec="$OPTARG"
      ;;
    d)
      config_dir="$OPTARG"
      ;;
    f)
      file_list="$OPTARG"
      ;;
    s)
      site_name="$OPTARG"
      ;;
    l)
      unset logger_tag
      log_file="$OPTARG"
      ;;
    \?)
      echo -e "Invalid option: -$OPTARG\n" >&2
      usage
      exit 1
      ;;
    :)
      echo -e "Option -$OPTARG requires an argument.\n" >&2
      usage
      exit 1
      ;;
  esac
done

# Set defaults to non-given argument variables
for variable in config_dir git_remote git_refspec log_file file_list site_name; do
  eval "if [ -z \"\$$variable\" ]; then
    $variable=\"\$default_$variable\"
  fi"
done

if [[ -e "config_sites.json" ]]; then
    if [ -n "$logger_tag" ]; then
      create_config_list "$site_name" "$file_list" \
        2>&1 | logger -t "$logger_tag"
    else
        create_config_list "$site_name" "$file_list" >> "$log_file" 2>&1
    fi
fi

cd "$config_dir"
if [ -n "$logger_tag" ]; then
  do_git_sync "$git_remote" "$git_refspec" "$file_list" "$logger_tag" \
    2>&1 | logger -t "$logger_tag"
else
  do_git_sync "$git_remote" "$git_refspec" "$file_list" "$logger_tag" \
    >> "$log_file" 2>&1
fi
git_head="$(git rev-parse HEAD 2>/dev/null)"
echo "GitHead = \"${git_head}\""

# Always exit successfully so that HTCondor configuration
# does not fail and HTCondor keeps running
exit 0
