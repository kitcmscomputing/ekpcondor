#!/bin/sh

# Shutdown voluntary or not?
#
# I'm not sure if /sbin/runlevel is available on systemd systems, so you
# check to make sure that this is normally nonzero and zero when shutting down.
SHUTDOWN=`/sbin/runlevel | /usr/bin/awk '{print $2}'`
SHUTDOWN_MESSAGE='because instance is being terminated'
if [ ${SHUTDOWN} -ne 0 ]; then
    SHUTDOWN_MESSAGE='for lack of work'
fi

MESSAGE="$(/bin/date) ${HOSTNAME} shutting down ${SHUTDOWN_MESSAGE}."

echo ${MESSAGE}

# Shut the machine down in 2 minutes.
/sbin/shutdown -h -m 2
# Shut down NOW
#~ /sbin/shutdown -h now
