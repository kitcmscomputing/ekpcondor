#!/usr/bin/env python
# ===============================================================================
# Copyright (c) 2016 by Frank Fischer
# ===============================================================================
from __future__ import unicode_literals, print_function, with_statement

import argparse
import json

# keys from JSON file
file_list_key = "config"
script_key = "scripts"
script_name = "name"
script_destination = "destination"


def main(file_, site, output):
    """Open JSON file and write a single key's values to an output file."""
    try:
        with open(name=file_, mode='r') as json_file:
            result = json.load(json_file)
    except IOError as err:
        print(err.strerror)
        exit(err.errno)
    try:
        with open(name=output, mode="w") as out_file:
            [out_file.write("%s\n" % entry) for entry in result[site][file_list_key]]
            [out_file.write("script:%s:scripts/%s\n" % (entry[script_destination], entry[script_name]))
             for entry in result[site][script_key]]
    except KeyError as err:
        if err.message in [site, file_list_key]:
            print("JSON file key error: %s" % err.message)
            exit(4)
        pass
    print("Content written to %s" % output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Open JSON file and extract a single JSON entry into a txt-file.")
    parser.add_argument("file", type=str, help="Absolute filename to the JSON file.")
    parser.add_argument("site", type=str, help="Site name (corresponding to JSON file keys).")
    parser.add_argument("--out", type=str, help="Output file name", default="/tmp/git_list")
    args = parser.parse_args()
    main(file_=args.file, site=args.site, output=args.out)
