# Readme

Different scripts, which can be useful for HTCondor in various ways.
Refer to the [HTCondor documentation](http://research.cs.wisc.edu/htcondor/manual/) how those scripts are invoked in detail.

In general they can be used by

    include command : *command*
or

    include : *script* |

Refer to the scripts' documentation string and/or inline code documentation for further information.

* * *

## Automatic configuration via GIT:
Pull configuration files from a remote repository.
Inspired by an [HTCondorWeek2016 presentation](https://research.cs.wisc.edu/htcondor/HTCondorWeek2016/presentations/Grasmick_GitConfig.pdf) by Erin Grasmick.

* `config_pull.sh`
* `config_sites.json`
    * Contains information which sites use which configuration file(s).
* `config_file_list.py`
    * Exports the corresponding JSON information into a txt file.

This later allows you to check your machines' configuration files with

        condor_status -master -const '!IsUndefined(GitHead)' -af name githead

### Setup:
For an easy condor setup, you can manually checkout these 3 files into a single folder and execute `config_pull.sh -s sitename`.

### Usage for auto pull:
1. the following has to be added to `/etc/sudoers.d/condor_git_config`:

        condor ALL=(root) NOPASSWD: /etc/condor/config_pull.sh
        Defaults!/etc/condor/config_pull.sh !requiretty

2. the following has to be added to `/etc/condor/condor_config`:

        json_site=""
        if $(IsMaster)
            # pull config from git and update Master attribute GitHead
            include command : /usr/bin/sudo /etc/condor/config_pull.sh -s $(json_site)
        endif

## Read walltime information
* `get_walltime.sh`

Read machine boot time & wall time from a file.

File content must be a single line containing the machine's wall time in unix time.
The file's creation time will be used to determine the machine's boot time.

## Automatic shutdown
* `shutdown.sh`

Determine the shutdown reason and shut down (with 2 minute delay). Originally from the [HTCondor Wiki](https://htcondor-wiki.cs.wisc.edu/index.cgi/wiki?p=HowToShutdownAnIdleMachine).

### Configuration
To automatically shut down a virtual machine when condor is idle for 5 minutes, the following has to be  added to `/etc/condor/condor_config`:

        # Tell HTCondor to gracefully exit if the condor_startd observes
        # that it has had no active claims for more than five consecutive minutes.
        STARTD_NOCLAIM_SHUTDOWN = 5 * $(MINUTE)

        # Optional:
        # condor_master exits if condor_startd is not running within 2 minutes
        MASTER.DAEMON_SHUTDOWN_FAST = ( STARTD_StartTime == 0 ) && ((CurrentTime - DaemonStartTime) > 2 * $(MINUTE))

        # Tell condor_master to run a script (as root) upon exit.
        DEFAULT_MASTER_SHUTDOWN_SCRIPT = /etc/condor/shutdown.sh
